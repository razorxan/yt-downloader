
const YoutubeDownloader = require('../index')

const a = new YoutubeDownloader({
    interval: 500,
    max_concurrent: 5
});
a.on('enqueue', item => {
    const dl = item.dl
    dl.on('progress', progress => {
        console.log(item.dl.meta.title, progress)
    })
}).add('https://www.youtube.com/watch?v=s3mQM0afZlE')
  .add('https://www.youtube.com/watch?v=CSvFpBOe8eY')
  .add('https://www.youtube.com/watch?v=iywaBOMvYLI')
    
process.stdin.resume();