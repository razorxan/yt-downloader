const path             = require('path')
const fs               = require('fs')
const { EventEmitter } = require('events')
const http             = require('http')
const uuidv4           = require('uuid/v4')
const ytdl             = require('youtube-dl')

class YoutubeDownload extends EventEmitter
{
    constructor (url, destination_path = process.cwd(), options = ['--format=18'])
    {

        super()
        this.url = url
        this.destination_path = destination_path
        this.options = options
        this.stream
        this.size
        this.cur_size = 0
        this.cur_perc = 0
        this.dl
        this.filename
        this.meta = {}
        this.id = uuidv4()
        this.status = 0 //0 not initialized, 1 started, 2 downloading, 3 paused, 4 completed, -1 aborted

    }

    start ()
    {

        return this.check().then(this.download.bind(this))

    }
	
    download (id)
    {

        const url = 'https://www.youtube.com/watch?v=' + id
        return new Promise((resolve, reject) => {
            this.emit('loading', url)
            this.dl = ytdl(url, this.options, {cwd: this.destination_path, maxBuffer: 1000 * 1024})
            this.dl.on('data', data => {
                if (this.status !== 2) this.status = 2
                this.cur_size += data.length
                let perc = (this.cur_size / this.size * 100).toFixed(1)
                if (this.cur_perc !== perc) {
                    this.cur_perc = perc
                    this.emit('progress', this.cur_perc)
                }
            })
            this.dl.on('error', error => {
				this.emit('error', error)
                reject(error)
            })
            this.dl.on('info', info => {
                this.meta = {
                    title: info.title,
                    duration: info.duration,
                    thumbnail: info.thumbnail,
                    id: info.id,
                    description: info.description
                }
                this.status = 1
                this.emit('start', {filename: info._filename, size: info.size, meta: this.meta})
                this.size = info.size
                this.filename = path.join(this.destination_path, info._filename)
                this.stream = fs.createWriteStream(this.filename)
                
                this.dl.pipe(this.stream)
                this.dl.on('end', () => {
                    resolve({filename: this.filename, size: this.size, meta: this.meta})
                    this.status = 4
                    this.emit('end', {filename: this.filename, size: this.size, meta: this.meta})
                })
                this.dl.on('close', () => {
                    this.emit('close')
                })
                this.dl.on('error', error => {
                    this.emit('error', error)
                })
            })
        })

    }
	
	stop ()
	{

        if (this.dl && this.dl.destroy) {
            this.dl.destroy()
            this.stream.destroy()
            fs.unlinkSync(this.filename)
            this.status = -1
            this.emit('aborted')
        }
        return this

	}
	
	pause ()
	{

        this.dl.pause()
        this.status = 3
        this.emit('pause')
        return this
        
	}

	resume ()
	{

        this.dl.resume()
        this.emit('resume')
        return this
        
    }
    
    check ()
    {

        const id = this.match(this.url) || this.url
        return new Promise((resolve, reject) => {
            const req = http.request({
                method: 'HEAD',
                protocolo: 'http',
                host: 'img.youtube.com',
                port: 80,
                path: '/vi/' + id + '/0.jpg'
            }, r => {
                if (r.statusCode === 200) resolve(id)
                else reject(id)
            })
            req.end()
        })

    }

    match (url)
    {

        const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
        const match = url.match(regExp)
        return (match&&match[7].length==11)? match[7] : false

    }

}

module.exports = YoutubeDownload