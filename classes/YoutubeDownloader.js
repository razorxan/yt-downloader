const path              = require('path')
const fs                = require('fs')
const { EventEmitter }  = require('events')
const uuidv4            = require('uuid/v4')
const YoutubeDownload   = require('./YoutubeDownload');

class YoutubeDownloader extends EventEmitter
{

    constructor (options = {})
    {

        super()
        this.queue = []
        this.downloads = []
        this.options = {
            max_concurrent: -1,
            interval: 1000,
            default_path: process.cwd(),
            youtubedl_args: ['--format=18'],
            ...options
        }
        this.max_concurrent = this.options.max_concurrent
        this.interval = setInterval(this.check.bind(this), this.options.interval)

    }
    
    startNext ()
    {

        if (this.queue.length > 0) {
            const download = this.queue.shift()
            download.dl.on('loading', url => {
                
            }).on('start', data => {
                
            }).on('progress', progress => {
                
            }).on('error', error => {
                
            }).on('end', data => {
                
            }).start()
            this.downloads.push(download)
        }
        
    }

    check ()
    {

        const current = this.downloads.filter(download => download.status < 3).length
        if ((this.max_concurrent < 0) || (current < this.max_concurrent && this.queue.length > 0)) {
            this.startNext()
        }
        return this

    }

    add (url)
    {

        const dl = new YoutubeDownload(url, this.options.default_path, this.options.youtubedl_args)
        const item = {
            id: uuidv4(),
            url,
            dl
        }
        this.queue.push(item)
        this.emit('enqueue', item)
        return this

    }

    _removeFromQueue (id)
    {

        for (let i = this.queue.length - 1; i > -1; i--) {
            if (this.queue[i].id === id) {
                this.queue.splice(i, 1)
            }
        }

    }

    _removeFromDownloads (id) {

        for (let i = this.downloads.length - 1; i > -1; i--) {
            if (this.downloads[i].id === id) {
                this.downloads.splice(i, 1)
            }
        }

    }

    remove (id) {
        
        this._removeFromQueue(id)
        this._removeFromDownloads(id)
        return this
        
    }

    cancel (id)
    {

        for (let i = 0; i < this.downloads.length; i++) {
            if (this.downloads[i].id === id) {
                this.downloads[i].dl.stop()
            }
        }

    }

}

module.exports = YoutubeDownloader